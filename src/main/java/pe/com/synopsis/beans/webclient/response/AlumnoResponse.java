package pe.com.synopsis.beans.webclient.response;

import pe.com.synopsis.beans.Alumno;

public class AlumnoResponse {

	private Alumno alumno;

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	
}
