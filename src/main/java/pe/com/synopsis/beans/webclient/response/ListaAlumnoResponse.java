package pe.com.synopsis.beans.webclient.response;

import java.util.List;

import pe.com.synopsis.beans.Alumno;

public class ListaAlumnoResponse {

	private List<Alumno> listaAlumno;

	public List<Alumno> getListaAlumno() 
	{
		return listaAlumno;
	}

	public void setListaAlumno(List<Alumno> listaAlumno) 
	{
		this.listaAlumno = listaAlumno;
	}
	
	
}
