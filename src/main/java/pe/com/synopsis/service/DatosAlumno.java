package pe.com.synopsis.service;

import java.util.List;

import pe.com.synopsis.beans.AlumnoWeb;

public interface DatosAlumno {

	public AlumnoWeb obtenerDatosAlumno();
	
	public List<AlumnoWeb> obtenerListaDeAlumnos();
}
