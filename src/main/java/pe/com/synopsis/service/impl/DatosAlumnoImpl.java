package pe.com.synopsis.service.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import pe.com.synopsis.beans.Alumno;
import pe.com.synopsis.beans.AlumnoWeb;
import pe.com.synopsis.beans.webclient.response.AlumnoResponse;
import pe.com.synopsis.beans.webclient.response.ListaAlumnoResponse;
import pe.com.synopsis.service.DatosAlumno;

@Service
public class DatosAlumnoImpl implements DatosAlumno {

	@Override
	public AlumnoWeb obtenerDatosAlumno() {
		AlumnoWeb a = null;

		RestTemplate restTemplate = new RestTemplate();

		try {
			String url = "http://localhost:8566/training/alumno/datosAlumno";
			Map<String, String> parametros = new HashMap<String, String>();

			HttpHeaders headers = new HttpHeaders();

			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			Map<String, String> map = new HashMap<String, String>();

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);

			builder.queryParam("codigo", 3);

			HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);

			ResponseEntity<AlumnoResponse> respuesta = restTemplate.exchange(builder.buildAndExpand(parametros).toUri(),
					HttpMethod.GET, entity, AlumnoResponse.class);

			if (respuesta.getStatusCode().equals(HttpStatus.OK)) {
				AlumnoResponse alumnoResponse = respuesta.getBody();

				a = new AlumnoWeb(alumnoResponse.getAlumno().getNombre(), alumnoResponse.getAlumno().getApellido(),
						alumnoResponse.getAlumno().getEdad());

			}

		} catch (Exception e) {

		}
		return a;
	}

	@Override
	public List<AlumnoWeb> obtenerListaDeAlumnos() {

		List<AlumnoWeb> listaAlumnos = new ArrayList<AlumnoWeb>();

		RestTemplate restTemplate = new RestTemplate();

		try {
			String url = "http://localhost:8566/training/alumno/listarAlumno";
			URI uri = new URI(url);
			HttpHeaders headers = new HttpHeaders();

			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			Map<String, String> map = new HashMap<String, String>();
			HttpEntity<Map<String, String>> entity = new HttpEntity<>(map, headers);

			ResponseEntity<ListaAlumnoResponse> respuesta = restTemplate.exchange(uri, HttpMethod.GET, entity,
					ListaAlumnoResponse.class);

			if (respuesta.getStatusCode().equals(HttpStatus.OK)) {
				ListaAlumnoResponse alumnoResponse = respuesta.getBody();

				for (Iterator<Alumno> iterator = alumnoResponse.getListaAlumno().iterator(); iterator.hasNext();) {
					Alumno alumno = (Alumno) iterator.next();
					AlumnoWeb a = new AlumnoWeb(alumno.getNombre(), alumno.getApellido(), alumno.getEdad());
					listaAlumnos.add(a);
				}

			}

		} catch (Exception e) {

		}

		return listaAlumnos;
	}

}
